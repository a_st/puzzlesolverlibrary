import {PuzzleSlot} from "../../puzzle-slot";

export type SUDOKU_SLOT_VALUE = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9;

export class SudokuPuzzleSlot extends PuzzleSlot {
    public x: SUDOKU_SLOT_VALUE;
    public y: SUDOKU_SLOT_VALUE;

    constructor(x: SUDOKU_SLOT_VALUE, y: SUDOKU_SLOT_VALUE) {
        super();
        this.x = x;
        this.y = y;
    }

    equals(slot: SudokuPuzzleSlot): boolean {
        return this.x == slot.x && this.y === slot.y;
    }

}
