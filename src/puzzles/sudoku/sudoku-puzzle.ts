import {Puzzle} from "../../puzzle";
import {SUDOKU_SLOT_VALUE, SudokuPuzzleSlot} from "./sudoku-puzzle-slot";
import {SUDOKU_PUZZLE_POSSIBLE_VALUE, SudokuPuzzleValue} from "./sudoku-puzzle-value";
import {PuzzleField} from "../../puzzle-field";
import {PuzzleConstraint} from "../../puzzle-constraint";
import {SudokuConstraint} from "./sudoku-constraint";

export class SudokuPuzzle extends Puzzle<SudokuPuzzleSlot, SudokuPuzzleValue> {

    constructor(values: { slot: SudokuPuzzleSlot, value: SudokuPuzzleValue }[]) {
        let puzzleField = new PuzzleField<SudokuPuzzleSlot, SudokuPuzzleValue>(SudokuPuzzle.getAvailableSlots());
        values.forEach(({slot, value}) => puzzleField.setValue(slot, value))
        super(puzzleField, SudokuPuzzle.getConstraints());
        if (this.anyConstraintsViolated()) {
            throw Error("Invalid input values!");
        }
    }

    private static getAvailableSlots(): SudokuPuzzleSlot[] {
        let slots = [];
        for (let y = 1; y <= 9; y++) {
            for (let x = 1; x <= 9; x++) {
                slots.push(new SudokuPuzzleSlot(x as SUDOKU_SLOT_VALUE, y as SUDOKU_SLOT_VALUE));
            }
        }
        return slots;
    }

    private static getConstraints(): PuzzleConstraint[] {
        return [
            new SudokuConstraint(),
        ];
    }

    public static fromArrayField(array_field: number[][]) {
        const values = [];
        for (let x = 0; x < 9; x++) {
            for (let y = 0; y < 9; y++) {
                if (array_field[y][x] != 0) {
                    values.push({
                        slot: new SudokuPuzzleSlot((x + 1) as SUDOKU_SLOT_VALUE, (y + 1) as SUDOKU_SLOT_VALUE),
                        value: new SudokuPuzzleValue(array_field[y][x] as SUDOKU_PUZZLE_POSSIBLE_VALUE)
                    })
                }
            }
        }
        return new SudokuPuzzle(values);
    }

    firstValue(): SudokuPuzzleValue {
        return new SudokuPuzzleValue(1);
    }

    isLastValue(value: SudokuPuzzleValue): boolean {
        return value.numberValue === 9;
    }

    nextValue(value: SudokuPuzzleValue): SudokuPuzzleValue {
        value.numberValue++; // dangerous!
        return value;
    }
}

