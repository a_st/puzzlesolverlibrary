import {PuzzleConstraint} from "../../puzzle-constraint";
import {SUDOKU_SLOT_VALUE, SudokuPuzzleSlot} from "./sudoku-puzzle-slot";
import {PuzzleField} from "../../puzzle-field";
import {SudokuPuzzleValue} from "./sudoku-puzzle-value";
import {PuzzleGuess} from "../../puzzle-guess";

export class SudokuConstraint extends PuzzleConstraint {
    isGuessValid(field: PuzzleField<SudokuPuzzleSlot, SudokuPuzzleValue>, guess: PuzzleGuess<SudokuPuzzleSlot, SudokuPuzzleValue>): boolean {
        if (this._values_in_col(guess.slot.x, field).indexOf(guess.value.numberValue) !== -1) {
            return false;
        }
        if (this._values_in_row(guess.slot.y, field).indexOf(guess.value.numberValue) !== -1) {
            return false;
        }
        if (this._values_in_area(guess.slot, field).indexOf(guess.value.numberValue) !== -1) {
            return false;
        }
        return true;
    }

    private _values_in_col(col: SUDOKU_SLOT_VALUE, field: PuzzleField<SudokuPuzzleSlot, SudokuPuzzleValue>): number[] {
        return field._field.filter(({slot, value}) => slot.x === col)
            .filter(v => v.value != null)
            .map(v => v.value!.numberValue) as number[];
    }

    private _values_in_row(row: SUDOKU_SLOT_VALUE, field: PuzzleField<SudokuPuzzleSlot, SudokuPuzzleValue>): number[] {
        return field._field.filter(({slot, value}) => slot.y === row)
            .filter(v => v.value != null)
            .map(v => v.value!.numberValue) as number[];
    }

    private _values_in_area(slot: SudokuPuzzleSlot, field: PuzzleField<SudokuPuzzleSlot, SudokuPuzzleValue>): number[] {
        const x = slot.x - 1;
        const y = slot.y - 1;
        const ax = (x - (x % 3)) + 1;
        const ay = (y - (y % 3)) + 1;
        // ax and ay are the coordinates of the top left corner of the area
        return field._field.filter(({slot, value}) => {
            return slot.x >= ax && slot.x < ax + 3 && slot.y >= ay && slot.y < ay + 3;
        }).filter(v => v.value != null)
            .map(v => v.value!.numberValue) as number[];
    }

}
