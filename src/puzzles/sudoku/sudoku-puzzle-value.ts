import {PuzzleValue} from "../../puzzle-value";

export type SUDOKU_PUZZLE_POSSIBLE_VALUE = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9;

export class SudokuPuzzleValue extends PuzzleValue {
    public numberValue: SUDOKU_PUZZLE_POSSIBLE_VALUE;

    constructor(numberValue: SUDOKU_PUZZLE_POSSIBLE_VALUE) {
        super();
        this.numberValue = numberValue;
    }
}
