import {PuzzleConstraint} from "../../puzzle-constraint";
import {SUDOKU_SLOT_VALUE} from "../sudoku/sudoku-puzzle-slot";
import {PuzzleField} from "../../puzzle-field";
import {SudokuPuzzleValue} from "../sudoku/sudoku-puzzle-value";
import {PuzzleGuess} from "../../puzzle-guess";
import {Str8tsPuzzleSlot} from "./str8ts-puzzle-slot";

export class Str8tsRowConstraint extends PuzzleConstraint {
    isGuessValid(field: PuzzleField<Str8tsPuzzleSlot, SudokuPuzzleValue>, guess: PuzzleGuess<Str8tsPuzzleSlot, SudokuPuzzleValue>): boolean {
        if (guess.slot.border) return true;
        return this._values_in_row(guess.slot.y, field).indexOf(guess.value.numberValue) === -1;
    }

    private _values_in_row(row: SUDOKU_SLOT_VALUE, field: PuzzleField<Str8tsPuzzleSlot, SudokuPuzzleValue>): number[] {
        return field._field.filter(({slot}) => slot.y === row)
            .filter(v => v.value != null)
            .map(v => v.value!.numberValue) as number[];
    }

}
