import {PuzzleConstraint} from "../../puzzle-constraint";
import {PuzzleField} from "../../puzzle-field";
import {Str8tsPuzzleSlot} from "./str8ts-puzzle-slot";
import {SudokuPuzzleValue} from "../sudoku/sudoku-puzzle-value";
import {PuzzleGuess} from "../../puzzle-guess";

export class Str8tsPuzzleConstraint extends PuzzleConstraint {
    isGuessValid(field: PuzzleField<Str8tsPuzzleSlot, SudokuPuzzleValue>, guess: PuzzleGuess<Str8tsPuzzleSlot, SudokuPuzzleValue>): boolean {
        if(guess.slot.border) return true;
        let valuesInStr8tCol = this._values_in_str8t_col(guess.slot, field);
        if (valuesInStr8tCol.filter(s => s == null).length == 1) {
            valuesInStr8tCol[valuesInStr8tCol.indexOf(null)] = guess.value.numberValue;
            if (!this._in_sequence(valuesInStr8tCol as number[])) {
                return false;
            }
        }

        let valuesInStr8tRow = this._values_in_str8t_row(guess.slot, field);
        if (valuesInStr8tRow.filter(s => s == null).length == 1) {
            valuesInStr8tRow[valuesInStr8tRow.indexOf(null)] = guess.value.numberValue;
            if (!this._in_sequence(valuesInStr8tRow as number[])) {
                return false;
            }
        }
        return true;
    }

    private _in_sequence(arr: number[]): boolean {
        if (arr.length == 1) return true;
        let numbers = arr.sort();
        return numbers[numbers.length - 1] - numbers[0] === numbers.length - 1;
    }

    private _values_in_str8t_col(slot: Str8tsPuzzleSlot, field: PuzzleField<Str8tsPuzzleSlot, SudokuPuzzleValue>): (number | null)[] {
        const col = field._field
            .filter(s => s.slot.x === slot.x)
            .sort((a, b) => a.slot.y - b.slot.y);
        return this._values__in_str8t_array(slot.y - 1, col);
    }

    private _values_in_str8t_row(slot: Str8tsPuzzleSlot, field: PuzzleField<Str8tsPuzzleSlot, SudokuPuzzleValue>): (number | null)[] {
        const row = field._field
            .filter(s => s.slot.y === slot.y)
            .sort((a, b) => a.slot.x - b.slot.x);
        return this._values__in_str8t_array(slot.x - 1, row);
    }

    private _values__in_str8t_array(index: number, arr: { slot: Str8tsPuzzleSlot, value: SudokuPuzzleValue | null }[]): (number | null)[] {
        let start = index;
        while (start > 0 && !arr[start - 1].slot.border) {
            start--;
        }
        let end = index;
        while (end < 8 && !arr[end + 1].slot.border) {
            end++;
        }
        return arr.slice(start, end + 1).map(s => s.value == null ? null : s.value.numberValue);
    }
}
