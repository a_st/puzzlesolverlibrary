import {Puzzle} from "../../puzzle";
import {SUDOKU_SLOT_VALUE} from "../sudoku/sudoku-puzzle-slot";
import {PuzzleField} from "../../puzzle-field";
import {PuzzleConstraint} from "../../puzzle-constraint";
import {SUDOKU_PUZZLE_POSSIBLE_VALUE, SudokuPuzzleValue} from "../sudoku/sudoku-puzzle-value";
import {Str8tsPuzzleSlot} from "./str8ts-puzzle-slot";
import {Str8tsPuzzleConstraint} from "./str8ts-puzzle-constraint";
import {Str8tsColumnConstraint} from "./str8ts-column-constraint";
import {Str8tsRowConstraint} from "./str8ts-row-constraint";

export class Str8tsPuzzle extends Puzzle<Str8tsPuzzleSlot, SudokuPuzzleValue> {

    constructor(values: { slot: Str8tsPuzzleSlot, value: SudokuPuzzleValue }[], borders: { x: number, y: number }[]) {
        let puzzleField = new PuzzleField<Str8tsPuzzleSlot, SudokuPuzzleValue>(Str8tsPuzzle.getAvailableSlots(borders));
        values.forEach(({slot, value}) => puzzleField.setValue(slot, value))
        super(puzzleField, Str8tsPuzzle.getConstraints());
        if (this.anyConstraintsViolated()) {
            throw Error("Invalid input values!");
        }
    }

    isSolved(): boolean {
        // a puzzle is solved if no constraints are violated and all slots are filled
        if (!this.field.getSubset(s => !s.border).isFull()) return false;
        if (this.anyConstraintsViolated()) {
            return false
        }
        return true;
    }

    getNextFreeSlot(): Str8tsPuzzleSlot | null {
        for (const slot of this.field.AVAILABLE_SLOTS.filter(s => !s.border)) {
            if (this.field.getValue(slot) == null) return slot;
        }
        return null;
    }

    firstValue(): SudokuPuzzleValue {
        return new SudokuPuzzleValue(1);
    }

    isLastValue(value: SudokuPuzzleValue): boolean {
        return value.numberValue === 9;
    }

    nextValue(value: SudokuPuzzleValue): SudokuPuzzleValue {
        value.numberValue++;
        return value;
    }

    private static getAvailableSlots(borders: { x: number, y: number }[]): Str8tsPuzzleSlot[] {
        const isBorder = (x: number, y: number) => borders.findIndex(b => b.x === x && b.y === y) !== -1;
        let slots = [];
        for (let y = 1; y <= 9; y++) {
            for (let x = 1; x <= 9; x++) {
                slots.push(new Str8tsPuzzleSlot(x as SUDOKU_SLOT_VALUE, y as SUDOKU_SLOT_VALUE, isBorder(x, y)));
            }
        }
        return slots;
    }

    private static getConstraints(): PuzzleConstraint[] {
        return [
            new Str8tsColumnConstraint(),
            new Str8tsRowConstraint(),
            new Str8tsPuzzleConstraint()
        ];
    }

    /**
     * 0 = no value (white cell)
     * 10 = no value (black cell)
     * x = x value (white)
     * 1x = x value (black cell)
     * @param array_field
     */
    public static fromArrayField(array_field: number[][]) {
        const borders: { x: number, y: number; }[] = [];
        const values = [];
        for (let x = 0; x < 9; x++) {
            for (let y = 0; y < 9; y++) {
                let number = array_field[y][x];
                let border = false;
                if (number >= 10) {
                    number -= 10;
                    borders.push({x: x + 1, y: y + 1});
                    border = true;
                }
                if (number != 0) {
                    values.push({
                        slot: new Str8tsPuzzleSlot((x + 1) as SUDOKU_SLOT_VALUE, (y + 1) as SUDOKU_SLOT_VALUE, border),
                        value: new SudokuPuzzleValue(number as SUDOKU_PUZZLE_POSSIBLE_VALUE)
                    })
                }
            }
        }
        return new Str8tsPuzzle(values, borders);
    }


    toLoggableObject(): any {
        let field: number[][] = [[], [], [], [], [], [], [], [], []]

        for (let y = 0; y < 9; y++) {
            for (let x = 0; x < 9; x++) {
                let value = this.field.getValue(new Str8tsPuzzleSlot(x + 1 as SUDOKU_SLOT_VALUE, y + 1 as SUDOKU_SLOT_VALUE, false));
                field[y][x] = value == null ? 0 : value!.numberValue
            }
        }
        return field;
    }
}
