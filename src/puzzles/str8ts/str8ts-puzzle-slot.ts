import {SUDOKU_SLOT_VALUE, SudokuPuzzleSlot} from "../sudoku/sudoku-puzzle-slot";

export class Str8tsPuzzleSlot extends SudokuPuzzleSlot {
    public border: boolean;

    constructor(x: SUDOKU_SLOT_VALUE, y: SUDOKU_SLOT_VALUE, border = false) {
        super(x, y);
        this.border = border;
    }
}
