import {PuzzleConstraint} from "../../puzzle-constraint";
import {PuzzleField} from "../../puzzle-field";
import {OneAndNinePuzzleSlot} from "./one-and-nine-puzzle-slot";
import {PuzzleGuess} from "../../puzzle-guess";
import {OneAndNinePuzzleValue} from "./one-and-nine-puzzle-value";
import {SUDOKU_SLOT_VALUE, SudokuPuzzleSlot} from "../sudoku/sudoku-puzzle-slot";

export class OneAndNinePuzzleConstraint extends PuzzleConstraint {
    isGuessValid(field: PuzzleField<OneAndNinePuzzleSlot, OneAndNinePuzzleValue>, guess: PuzzleGuess<OneAndNinePuzzleSlot, OneAndNinePuzzleValue>): boolean {
        if(guess.slot.oneAndNineClue != null) return true;

        let ROW_CLUE_SLOT_VALUE = field.getValue(new OneAndNinePuzzleSlot(1, guess.slot.y as SUDOKU_SLOT_VALUE, "ROW_CLUE"));
        if (ROW_CLUE_SLOT_VALUE != null) {
            let numbers = this._values_in_row(guess.slot.y, field);
            numbers[guess.slot.x - 1] = guess.value.numberValue;
            if (this._contains_one_and_nine_and_no_gaps_between(numbers)) {
                const sum = this._sum_between_one_and_nine(numbers as number[]);
                if (ROW_CLUE_SLOT_VALUE.numberValue != sum) {
                    return false;
                }
            }
        }

        let COL_CLUE_SLOT_VALUE = field.getValue(new OneAndNinePuzzleSlot(guess.slot.x as SUDOKU_SLOT_VALUE, 1, "COLUMN_CLUE"));
        if (COL_CLUE_SLOT_VALUE != null) {
            let numbers = this._values_in_col(guess.slot.x, field);
            numbers[guess.slot.y - 1] = guess.value.numberValue;
            if (this._contains_one_and_nine_and_no_gaps_between(numbers)) {
                const sum = this._sum_between_one_and_nine(numbers as number[]);
                if (COL_CLUE_SLOT_VALUE.numberValue != sum) {
                    return false;
                }
            }
        }
        return true;
    }

    private _values_in_col(col: SUDOKU_SLOT_VALUE, field: PuzzleField<OneAndNinePuzzleSlot, OneAndNinePuzzleValue>): (number | null)[] {
        return field._field.filter(({slot, value}) => slot.x === col && slot.oneAndNineClue == null)
            .sort((a, b) => a.slot.y - b.slot.y)
            .map(v => v.value == null ? null : v.value!.numberValue);
    }

    private _values_in_row(row: SUDOKU_SLOT_VALUE, field: PuzzleField<OneAndNinePuzzleSlot, OneAndNinePuzzleValue>): (number | null)[] {
        return field._field.filter(({slot, value}) => slot.y === row && slot.oneAndNineClue == null)
            .sort((a, b) => a.slot.x - b.slot.x)
            .map(v => v.value == null ? null : v.value!.numberValue);
    }

    private _contains_one_and_nine_and_no_gaps_between(numbers: (number | null)[]): boolean {
        let s = numbers.indexOf(1);
        if (s == -1) return false;
        let e = numbers.indexOf(9);
        if (e == -1) return false;
        if (s > e) {
            const t = s;
            s = e;
            e = t;
        }
        return numbers.filter((v, i) => i >= s && i <= e).filter(s => s == null).length === 0;
    }

    private _numbers_between_one_and_nine(numbers: number[]): number[] {
        let s = numbers.indexOf(1);
        let e = numbers.indexOf(9);
        if (s > e) {
            const t = s;
            s = e;
            e = t;
        }
        return numbers.filter((v, i) => i > s && i < e);
    }

    private _sum_between_one_and_nine(numbers: number[]): number {
        let numbers1 = this._numbers_between_one_and_nine(numbers);
        return numbers1.reduce((p, c) => p! + c!, 0);
    }
}
