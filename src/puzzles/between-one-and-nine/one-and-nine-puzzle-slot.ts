import {SUDOKU_SLOT_VALUE, SudokuPuzzleSlot} from "../sudoku/sudoku-puzzle-slot";

export type CLUE = "ROW_CLUE" | "COLUMN_CLUE" | null;

export class OneAndNinePuzzleSlot extends SudokuPuzzleSlot {
    public oneAndNineClue: CLUE;

    constructor(x: SUDOKU_SLOT_VALUE, y: SUDOKU_SLOT_VALUE, oneAndNineClue: CLUE = null) {
        super(x, y);
        this.oneAndNineClue = oneAndNineClue;
    }

    equals(slot: OneAndNinePuzzleSlot): boolean {
        return this.oneAndNineClue == slot.oneAndNineClue && super.equals(slot);
    }
}
