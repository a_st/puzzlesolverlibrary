import {SUDOKU_PUZZLE_POSSIBLE_VALUE, SudokuPuzzleValue} from "../sudoku/sudoku-puzzle-value";
import {PuzzleValue} from "../../puzzle-value";

export class OneAndNinePuzzleValue extends PuzzleValue{
    public numberValue: number;

    constructor(numberValue: number) {
        super();
        this.numberValue = numberValue;
    }
}
