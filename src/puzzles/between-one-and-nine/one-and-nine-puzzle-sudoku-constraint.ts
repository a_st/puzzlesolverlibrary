import {SudokuConstraint} from "../sudoku/sudoku-constraint";
import {PuzzleField} from "../../puzzle-field";
import {OneAndNinePuzzleSlot} from "./one-and-nine-puzzle-slot";
import {PuzzleGuess} from "../../puzzle-guess";
import {SudokuPuzzleValue} from "../sudoku/sudoku-puzzle-value";
import {SudokuPuzzleSlot} from "../sudoku/sudoku-puzzle-slot";

export class OneAndNinePuzzleSudokuConstraint extends SudokuConstraint {

    isGuessValid(field: PuzzleField<OneAndNinePuzzleSlot, any>, guess: PuzzleGuess<OneAndNinePuzzleSlot, any>): boolean {
        if(guess.slot.oneAndNineClue != null) return true;
        return super.isGuessValid(field.getSubset<SudokuPuzzleSlot, SudokuPuzzleValue>((slot) => slot.oneAndNineClue === null), guess as PuzzleGuess<OneAndNinePuzzleSlot, SudokuPuzzleValue>);
    }
}
