import {Puzzle} from "../../puzzle";
import {OneAndNinePuzzleSlot} from "./one-and-nine-puzzle-slot";
import {OneAndNinePuzzleValue} from "./one-and-nine-puzzle-value";
import {SUDOKU_SLOT_VALUE} from "../sudoku/sudoku-puzzle-slot";
import {PuzzleField} from "../../puzzle-field";
import {OneAndNinePuzzleSudokuConstraint} from "./one-and-nine-puzzle-sudoku-constraint";
import {OneAndNinePuzzleConstraint} from "./one-and-nine-puzzle-constraint";

export class OneAndNinePuzzle extends Puzzle<OneAndNinePuzzleSlot, OneAndNinePuzzleValue> {

    constructor(values: { slot: OneAndNinePuzzleSlot, value: OneAndNinePuzzleValue }[]) {
        let puzzleField = new PuzzleField<OneAndNinePuzzleSlot, OneAndNinePuzzleValue>(OneAndNinePuzzle.getAvailableSlots());
        values.forEach(({slot, value}) => puzzleField.setValue(slot, value))
        super(puzzleField, OneAndNinePuzzle.getConstraints());
        if (this.anyConstraintsViolated()) {
            throw Error("Invalid input values!");
        }
    }

    firstValue(): OneAndNinePuzzleValue {
        return new OneAndNinePuzzleValue(1);
    }

    isLastValue(value: OneAndNinePuzzleValue): boolean {
        return value.numberValue === 9;
    }

    nextValue(value: OneAndNinePuzzleValue): OneAndNinePuzzleValue {
        value.numberValue++;
        return value;
    }

    getNextFreeSlot(): OneAndNinePuzzleSlot | null {
        for (const slot of this.field.AVAILABLE_SLOTS.filter(s => s.oneAndNineClue == null)) {
            if (this.field.getValue(slot) == null) return slot;
        }
        return null;
    }

    isSolved(): boolean {
        // a puzzle is solved if no constraints are violated and all slots are filled
        if (!this.field.getSubset((s) => s.oneAndNineClue == null).isFull()) return false;
        if (this.anyConstraintsViolated()) {
            return false
        }
        return true;
    }

    private static getAvailableSlots() {
        let slots = [];
        for (let y = 1; y <= 9; y++) {
            for (let x = 1; x <= 9; x++) {
                slots.push(new OneAndNinePuzzleSlot(x as SUDOKU_SLOT_VALUE, y as SUDOKU_SLOT_VALUE));
            }
        }
        for (let x = 1; x <= 9; x++) {
            slots.push(new OneAndNinePuzzleSlot(x as SUDOKU_SLOT_VALUE, 1, "COLUMN_CLUE"));
        }
        for (let y = 1; y <= 9; y++) {
            slots.push(new OneAndNinePuzzleSlot(1, y as SUDOKU_SLOT_VALUE, "ROW_CLUE"));
        }
        return slots;
    }

    private static getConstraints() {
        return [
            new OneAndNinePuzzleSudokuConstraint(),
            new OneAndNinePuzzleConstraint()
        ];
    }

    public static fromArrayField(array_field: number[][], row_clues: (number | null)[], col_clues: (number | null)[]) {
        const values: { slot: OneAndNinePuzzleSlot, value: OneAndNinePuzzleValue }[] = [];
        for (let x = 0; x < 9; x++) {
            for (let y = 0; y < 9; y++) {
                if (array_field[y][x] != 0) {
                    values.push({
                        slot: new OneAndNinePuzzleSlot((x + 1) as SUDOKU_SLOT_VALUE, (y + 1) as SUDOKU_SLOT_VALUE),
                        value: new OneAndNinePuzzleValue(array_field[y][x])
                    })
                }
            }
        }
        for (let x = 0; x < 9; x++) {
            if (col_clues[x] != null)
                values.push({
                    slot: new OneAndNinePuzzleSlot((x + 1) as SUDOKU_SLOT_VALUE, 1, "COLUMN_CLUE"),
                    value: new OneAndNinePuzzleValue(col_clues[x]!)
                })
        }

        for (let y = 0; y < 9; y++) {
            if (row_clues[y] != null)
                values.push({
                    slot: new OneAndNinePuzzleSlot(1, (y + 1) as SUDOKU_SLOT_VALUE, "ROW_CLUE"),
                    value: new OneAndNinePuzzleValue(row_clues[y]!)
                })
        }

        return new OneAndNinePuzzle(values);
    }
}
