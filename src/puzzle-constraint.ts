import {PuzzleField} from "./puzzle-field";
import {PuzzleGuess} from "./puzzle-guess";

/**
 * Expects fields to be valid already!
 */
export abstract class PuzzleConstraint {

    abstract isGuessValid(field: PuzzleField<any, any>, guess: PuzzleGuess<any, any>): boolean;

    /**
     * Very consumption intensive!
     * @param field
     */
    public isViolated(field: PuzzleField<any, any>) {
        for (const slot of field.AVAILABLE_SLOTS) {
            const v = field.getValue(slot);
            if (v != null) {
                field.clearValue(slot);
                if (!this.isGuessValid(field, new PuzzleGuess(slot, v))) {
                    return true;
                }
                field.setValue(slot, v);
            }
        }
        return false;
    }

}
