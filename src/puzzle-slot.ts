/**
 * Represents a possible slot for a guess.
 * In Sudoku-Logic this is a cell, represented by its x and y coordinates
 */
export abstract class PuzzleSlot {
    abstract equals(slot: PuzzleSlot): boolean;
}
