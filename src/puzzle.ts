import {PuzzleGuess} from "./puzzle-guess";
import {PuzzleSlot} from "./puzzle-slot";
import {PuzzleValue} from "./puzzle-value";
import {PuzzleConstraint} from "./puzzle-constraint";
import {PuzzleField} from "./puzzle-field";

export abstract class Puzzle<SLOT extends PuzzleSlot, VALUE extends PuzzleValue> {

    public field!: PuzzleField<SLOT, VALUE>;
    public constraints!: PuzzleConstraint[];

    protected constructor(field: PuzzleField<SLOT, VALUE>, constraints: PuzzleConstraint[]) {
        this.field = field;
        this.constraints = constraints;
    }

    public setValue(slot: SLOT, value: VALUE) {
        this.field.setValue(slot, value);
    };

    public isSolved(): boolean {
        // a puzzle is solved if no constraints are violated and all slots are filled
        if (!this.field.isFull()) return false;
        if (this.anyConstraintsViolated()) {
            return false
        }
        return true;
    }

    public anyConstraintsViolated(): boolean {
        for (const constraint of this.constraints) {
            if (constraint.isViolated(this.field)) return true;
        }
        return false;
    }


    public getNextFreeSlot(): SLOT | null {
        for (const slot of this.field.AVAILABLE_SLOTS) {
            if (this.field.getValue(slot) == null) return slot;
        }
        return null;
    }

    public clearSlot(slot: SLOT): void {
        this.field.clearValue(slot);
    };

    public isValidGuess(guess: PuzzleGuess<SLOT, VALUE>): boolean {
        for (const constraint of this.constraints) {
            if (!constraint.isGuessValid(this.field, guess)) return false;
        }
        return true;
    }

    public abstract firstValue(): VALUE;

    public abstract isLastValue(value: VALUE): boolean;

    public abstract nextValue(value: VALUE): VALUE;

    public toLoggableObject(): any {
        return "not implemented";
    }

}
