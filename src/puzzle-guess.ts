import {PuzzleSlot} from "./puzzle-slot";
import {PuzzleValue} from "./puzzle-value";

export class PuzzleGuess<SLOT extends PuzzleSlot, VALUE extends PuzzleValue> {

    public slot: SLOT;
    public value: VALUE;

    constructor(slot: SLOT, value: VALUE) {
        this.slot = slot;
        this.value = value;
    }
}
