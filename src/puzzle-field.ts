import {PuzzleSlot} from "./puzzle-slot";
import {PuzzleValue} from "./puzzle-value";

export class PuzzleField<SLOT extends PuzzleSlot, VALUE extends PuzzleValue> {

    constructor(availableSlots: SLOT[]) {
        this.AVAILABLE_SLOTS = availableSlots;
        for (const availableSlot of availableSlots) {
            this._field.push({slot: availableSlot, value: null})
        }
    }

    public _field: { slot: SLOT, value: VALUE | null; }[] = [];

    public AVAILABLE_SLOTS!: SLOT[];

    public getValue(slot: SLOT): VALUE | null {
        return this.getFieldElement(slot).value;
    }

    public setValue(slot: SLOT, value: VALUE | null) {
        this.getFieldElement(slot).value = value;
    }

    public clearValue(slot: SLOT) {
        this.setValue(slot, null);
    }

    public isFull(): boolean {
        return this._field.find(({value}) => value == null) == null;
    }

    public getSubset<SLOT2 extends PuzzleSlot, VALUE2 extends PuzzleValue>(predicate: (slot: SLOT) => boolean): PuzzleField<SLOT2, VALUE2> {
        let puzzleField = new PuzzleField<SLOT2, VALUE2>(this.AVAILABLE_SLOTS.filter(predicate).map(s => s as unknown as SLOT2));
        puzzleField._field = [...this._field.filter(({slot}) => predicate(slot)).map((fieldValue) => {
            const n = {...fieldValue as unknown as { slot: SLOT2, value: VALUE2; }};
            n.value = fieldValue.value as unknown as VALUE2;
            return n;
        })];
        return puzzleField;
    }


    private getFieldElement(s: SLOT) {
        return this._field[this._field.findIndex(({slot}) => slot.equals(s))];
    }


}

