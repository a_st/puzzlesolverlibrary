import {Puzzle} from "./puzzle";
import {PuzzleSlot} from "./puzzle-slot";
import {PuzzleValue} from "./puzzle-value";
import {PuzzleGuess} from "./puzzle-guess";

export class PuzzleSolver<PUZZLE extends Puzzle<PuzzleSlot, PuzzleValue>> {

    public solvePuzzle(puzzle: PUZZLE): boolean {
        const guesses: PuzzleGuess<PuzzleSlot, PuzzleValue>[] = [];
        while (!puzzle.isSolved()) {
            let nextFreeSlot = puzzle.getNextFreeSlot();
            if (nextFreeSlot == null) {
                return false;
            }
            let guess = new PuzzleGuess(nextFreeSlot, puzzle.firstValue())
            while (!puzzle.isValidGuess(guess)) {
                while (puzzle.isLastValue(guess.value)) {
                    if (guesses.length === 0) {
                        return false;
                    }
                    guess = guesses.pop()!;
                    puzzle.clearSlot(guess.slot);
                }
                guess.value = puzzle.nextValue(guess.value);
            }
            puzzle.setValue(guess.slot, guess.value);
            guesses.push(guess);
        }
        return true;
    }
}
