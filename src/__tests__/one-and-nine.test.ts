import {SUDOKU_SLOT_VALUE} from "../puzzles/sudoku/sudoku-puzzle-slot";
import {PuzzleSolver} from "../puzzle-solver";
import {OneAndNinePuzzle} from "../puzzles/between-one-and-nine/one-and-nine-puzzle";
import {OneAndNinePuzzleSlot} from "../puzzles/between-one-and-nine/one-and-nine-puzzle-slot";

test('OneAndNineSolver', () => {
    let sudokuPuzzle = OneAndNinePuzzle.fromArrayField([
        [0, 0, 6, 0, 0, 0, 0, 0, 0],
        [0, 9, 0, 3, 0, 0, 0, 0, 0],
        [2, 0, 0, 0, 1, 0, 0, 0, 0],
        [0, 7, 0, 1, 0, 0, 0, 0, 0],
        [0, 0, 5, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
    ], [11, 27, 4, 0, 7, 5, 6, 28, 6], [13, 0, 8, 0, 2, 8, 15, 0, 13]);
    let sudokuSolver = new PuzzleSolver();
    let result = sudokuSolver.solvePuzzle(sudokuPuzzle);
    expect(result).toBeTruthy();

    // let expectedResult: number[][] = [[],[],[],[],[],[],[],[],[]]
    //
    // for (let y = 0; y < 9; y++) {
    //     for (let x = 0; x < 9; x++) {
    //         let value = sudokuPuzzle.field.getValue(new OneAndNinePuzzleSlot(x + 1 as SUDOKU_SLOT_VALUE, y + 1 as SUDOKU_SLOT_VALUE));
    //         expectedResult[y][x] = value!.numberValue
    //     }
    // }
    // console.log(expectedResult);

    const expectedResult = [[3, 1, 6, 5, 9, 7, 2, 4, 8],
        [8, 9, 4, 3, 2, 6, 5, 7, 1],
        [2, 5, 7, 8, 1, 4, 9, 3, 6],
        [4, 7, 9, 1, 6, 5, 8, 2, 3],
        [1, 2, 5, 9, 8, 3, 7, 6, 4],
        [6, 8, 3, 4, 7, 2, 1, 5, 9],
        [7, 6, 1, 2, 4, 9, 3, 8, 5],
        [9, 3, 2, 6, 5, 8, 4, 1, 7],
        [5, 4, 8, 7, 3, 1, 6, 9, 2]];
    for (let y = 0; y < 9; y++) {
        for (let x = 0; x < 9; x++) {
            expect(sudokuPuzzle.field.getValue(new OneAndNinePuzzleSlot(x + 1 as SUDOKU_SLOT_VALUE, y + 1 as SUDOKU_SLOT_VALUE))!.numberValue)
                .toBe(expectedResult[y][x])
        }
    }
});
