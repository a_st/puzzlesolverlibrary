import {SUDOKU_SLOT_VALUE} from "../puzzles/sudoku/sudoku-puzzle-slot";
import {PuzzleSolver} from "../puzzle-solver";
import {Str8tsPuzzle} from "../puzzles/str8ts/str8ts-puzzle";
import {Str8tsPuzzleSlot} from "../puzzles/str8ts/str8ts-puzzle-slot";

test('Str8ts', () => {
    let puzzle = Str8tsPuzzle.fromArrayField([
        [10, 10, 0, 0, 19, 0, 0, 0, 0],
        [11, 0, 0, 0, 0, 0, 5, 0, 0],
        [0, 0, 3, 10, 0, 0, 0, 10, 10],
        [0, 0, 10, 0, 0, 16, 10, 1, 0],
        [0, 18, 10, 0, 0, 10, 0, 0, 0],
        [10, 10, 0, 0, 10, 0, 0, 17, 10],
        [12, 0, 0, 15, 0, 0, 10, 9, 0],
        [0, 0, 0, 0, 0, 0, 2, 0, 0],
        [0, 7, 0, 0, 10, 0, 0, 10, 13],
    ]);
    let sudokuSolver = new PuzzleSolver();
    let result = sudokuSolver.solvePuzzle(puzzle);
    expect(result).toBeTruthy();

    const expectedResult = [[0, 0, 2, 1, 9, 8, 7, 5, 6],
        [1, 3, 4, 2, 8, 9, 5, 6, 7],
        [4, 2, 3, 0, 5, 7, 6, 0, 0],
        [3, 4, 0, 8, 7, 6, 0, 1, 2],
        [5, 8, 0, 7, 6, 0, 3, 2, 1],
        [0, 0, 8, 9, 0, 3, 4, 7, 0],
        [2, 6, 7, 5, 3, 4, 0, 9, 8],
        [7, 5, 6, 3, 4, 1, 2, 8, 9],
        [6, 7, 5, 4, 0, 2, 1, 0, 3]];
    for (let y = 0; y < 9; y++) {
        for (let x = 0; x < 9; x++) {
            let value = puzzle.field.getValue(new Str8tsPuzzleSlot(x + 1 as SUDOKU_SLOT_VALUE, y + 1 as SUDOKU_SLOT_VALUE));
            expect(value == null ? 0 : value.numberValue)
                .toBe(expectedResult[y][x])
        }
    }
});
