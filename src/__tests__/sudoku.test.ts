import {SudokuPuzzle} from "../puzzles/sudoku/sudoku-puzzle";
import {SUDOKU_SLOT_VALUE, SudokuPuzzleSlot} from "../puzzles/sudoku/sudoku-puzzle-slot";
import {SUDOKU_PUZZLE_POSSIBLE_VALUE, SudokuPuzzleValue} from "../puzzles/sudoku/sudoku-puzzle-value";
import {PuzzleGuess} from "../puzzle-guess";
import {PuzzleSolver} from "../puzzle-solver";

test('Sudoku constraints', () => {
    function guess(x: SUDOKU_SLOT_VALUE, y: SUDOKU_SLOT_VALUE, val: SUDOKU_PUZZLE_POSSIBLE_VALUE) {
        return new PuzzleGuess<SudokuPuzzleSlot, SudokuPuzzleValue>(new SudokuPuzzleSlot(x, y), new SudokuPuzzleValue(val))
    }

    let sudokuPuzzle = SudokuPuzzle.fromArrayField([
        [0, 0, 0, 8, 0, 0, 0, 0, 0],
        [4, 0, 0, 0, 1, 5, 0, 3, 0],
        [0, 2, 9, 0, 4, 0, 5, 1, 8],
        [0, 4, 0, 0, 0, 0, 1, 2, 0],
        [0, 0, 0, 6, 0, 2, 0, 0, 0],
        [0, 3, 2, 0, 0, 0, 0, 9, 0],
        [6, 9, 3, 0, 5, 0, 8, 7, 0],
        [0, 5, 0, 4, 8, 0, 0, 0, 1],
        [0, 0, 0, 0, 0, 3, 0, 0, 0],
    ]);
    expect(sudokuPuzzle.anyConstraintsViolated()).toBeFalsy();
    expect(sudokuPuzzle.isSolved()).toBeFalsy();

    let guess1 = guess(1, 1, 8);
    expect(sudokuPuzzle.isValidGuess(guess1)).toBeFalsy(); // row violated
    expect(sudokuPuzzle.isValidGuess(guess(1, 1, 6))).toBeFalsy(); // column violated
    expect(sudokuPuzzle.isValidGuess(guess(1, 1, 2))).toBeFalsy(); // area violated

    expect(sudokuPuzzle.isValidGuess(guess(1, 1, 3))).toBeTruthy(); // This is just any truthy value
});

test('Sudoku solver', () => {
    let sudokuPuzzle = SudokuPuzzle.fromArrayField([
        [0, 0, 0, 8, 0, 0, 0, 0, 0],
        [4, 0, 0, 0, 1, 5, 0, 3, 0],
        [0, 2, 9, 0, 4, 0, 5, 1, 8],
        [0, 4, 0, 0, 0, 0, 1, 2, 0],
        [0, 0, 0, 6, 0, 2, 0, 0, 0],
        [0, 3, 2, 0, 0, 0, 0, 9, 0],
        [6, 9, 3, 0, 5, 0, 8, 7, 0],
        [0, 5, 0, 4, 8, 0, 0, 0, 1],
        [0, 0, 0, 0, 0, 3, 0, 0, 0],
    ]);
    let sudokuSolver = new PuzzleSolver();
    let result = sudokuSolver.solvePuzzle(sudokuPuzzle);
    expect(result).toBeTruthy();

    const expectedResult = [
        [3, 1, 5, 8, 2, 7, 9, 4, 6],
        [4, 6, 8, 9, 1, 5, 7, 3, 2],
        [7, 2, 9, 3, 4, 6, 5, 1, 8],
        [9, 4, 6, 5, 3, 8, 1, 2, 7],
        [5, 7, 1, 6, 9, 2, 4, 8, 3],
        [8, 3, 2, 1, 7, 4, 6, 9, 5],
        [6, 9, 3, 2, 5, 1, 8, 7, 4],
        [2, 5, 7, 4, 8, 9, 3, 6, 1],
        [1, 8, 4, 7, 6, 3, 2, 5, 9]
    ];
    for (let y = 0; y < 9; y++) {
        for (let x = 0; x < 9; x++) {
            expect(sudokuPuzzle.field.getValue(new SudokuPuzzleSlot(x + 1 as SUDOKU_SLOT_VALUE, y + 1 as SUDOKU_SLOT_VALUE))!.numberValue)
                .toBe(expectedResult[y][x])
        }
    }
});
